require_relative "base_api"

class Sessions < BaseApi
  def login(payload)
    #self eu tenho acesso aos objetos da propria classe
    return self.class.post(
             "/sessions",
             body: payload.to_json,
             headers: {
               "Content-Type": "application/json",
             },
           )
  end
end
