#encoding: utf-8

describe "GET /equipos/{equipo_id}" do
  #Pre condição para rodar os testes
  before(:all) do
    @payload = { name: "Bruno", email: "bruno@gmail.com", password: "pwd123" }
    MongoDB.new.remove_user(@payload[:email])
    result = Signup.new.create(@payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obeter unico equipo" do
    before(:all) do
      # dado que tenho um novo equipamento
      @payloadEquipo = {
        thumbnail: Helpers::get_thumb("pedais.jpg"),
        name: "Pedais do Tom Morello",
        category: "Áudio e Tecnologia".force_encoding("ASCII-8BIT"),
        price: 199,
      }
      MongoDB.new.remove_equipo(@payloadEquipo[:name], @user_id)

      # e eu tenho o id desse equipamento
      equipo = Equipos.new.create(@payloadEquipo, @user_id)
      @equipo_id = equipo.parsed_response["_id"]

      # quando for uma requisição uma requisição delete por id
      @result = Equipos.new.remove_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end

    after(:all) do
      #Como não estou utilizando uma conta já criada e criando uma nova eu chamo o remove equipo e remove user novamente
      MongoDB.new.remove_equipo(@payloadEquipo[:name], @user_id)
    end
  end

  context "equipo nao existe" do
    before(:all) do
      @result = Equipos.new.remove_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end
end
