describe "POST /sessions" do
  #gancho para poder realizarmos varias validações.
  context "login com sucesso" do
    #faz com que o before sejá executado uma unica vez
    before(:all) do
      #pega o usuario criado no banco automaticamente no before do spec_helper
      payload = { email: "betao@hotmail.com", password: "pwd123"}
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  # exemples = [
  #   {
  #     title: "senha incorreta",
  #     payload: { email: "brunosisag@gmail.com", password: "123456"},
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "usuario não existe",
  #     payload: { email: "404@gmail.com", password: "pwd123"},
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "campo email vazio",
  #     payload: { email: "", password: "123456"},
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "senha em branco",
  #     payload: { email: "brunosisag@gmail.com", password: ""},
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "sem o campo email",
  #     payload: { password: "pwd123"},
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "sem o campo senha",
  #     payload: { email: "brunosisag@gmail.com"},
  #     code: 412,
  #     error: "required password",
  #   },
  # ]

  # puts exemples.to_json

  exemples = Helpers::get_fixture("login")

  exemples.each do |e|
    context e[:title] do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida msg de erro #{e[:error]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
