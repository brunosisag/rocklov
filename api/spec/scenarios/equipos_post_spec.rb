describe "POST /equipos" do
  #Precondição para rodar os testes
  before(:all) do
    @payload = { name: "Bruno", email: "bruno@gmail.com", password: "pwd123" }
    MongoDB.new.remove_user(@payload[:email])
    result = Signup.new.create(@payload)
    @user_id = result.parsed_response["_id"]
  end

  context "novo equipo" do
    before(:all) do
      @payloadEquipo = {
        thumbnail: Helpers::get_thumb("kramer.jpg"),
        name: "Kramer Eddie Van Halen",
        category: "Cordas",
        price: 299,
      }

      MongoDB.new.remove_equipo(@payloadEquipo[:name], @user_id)

      @result = Equipos.new.create(@payloadEquipo, @user_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end

    after(:all) do
      #Como não estou utilizando uma conta já criada e criando uma nova eu chamo o remove equipo e remove user novamente
      MongoDB.new.remove_equipo(@payloadEquipo[:name], @user_id)
      MongoDB.new.remove_user(@payload[:email])
    end
  end

  context "não autorizado" do
    before(:all) do
      @payloadEquipo = {
        thumbnail: Helpers::get_thumb("baixo.jpg"),
        name: "Contra baixo",
        category: "Cordas",
        price: 299,
      }

      @result = Equipos.new.create(@payloadEquipo, nil)
    end

    it "deve retornar 401" do
      expect(@result.code).to eql 401
    end

    after(:all) do
      #Como não estou utilizando uma conta já criada e criando uma nova eu chamo o remove equipo e remove user novamente
      MongoDB.new.remove_user(@payload[:email])
    end
  end
end
