describe "POST /signup" do
  #gancho para poder realizarmos varias validações.
  context "novo usuario" do
    #faz com que o before sejá executado uma unica vez
    before(:all) do
      payload = { name: "Pitty", email: "pitty@gmail.com", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # dado que eu tenho um novo usuario
      payload = { name: "João da Silva", email: "joao@ig.com,.br", password: "pwd123"}
      MongoDB.new.remove_user(payload[:email])
      #e o email desse usuário ja foi cadastrado no sistema
      Signup.new.create(payload)

      #quando faço uma requisição para rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      # então deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end

    # name é obrigatorio
    # email é obrigatorio
    # passward é obrigatório
  end

  exemples = Helpers::get_fixture("cadastro")

  exemples.each do |e|
    context e[:title] do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida msg de erro #{e[:error]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
