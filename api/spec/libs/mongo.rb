require "mongo"

Mongo::Logger.logger = Logger.new("./logs/mongo.log")

class MongoDB
  attr_accessor :db_client, :users, :equipos

  def initialize
    @db_client = Mongo::Client.new("mongodb://rocklov-db:27017/rocklov")
    @users = @db_client[:users]
    @equipos = @db_client[:equipos]
  end

  def drop_danger
    @db_client.database.drop
  end

  def insert_user(docs)
    #inseri todos os usuaruis que estão na lista
    @users.insert_many(docs)
  end

  def remove_user(email)
    @users.delete_many({ email: email })
  end

  def get_user(email)
    user = @users.find({ email: email }).first
    return user[:_id]
  end

  def remove_equipo(name, user_id)
    # Se passarmos o user_id como string o mongo não vai deletar o objeto, para que seja feita o delete temos de passar o user_id como ObjectId
    obj_id = BSON::ObjectId.from_string(user_id)
    @equipos.delete_many({ name: name, user: obj_id })
  end

  #retorna um id no formato do mongoDB
  def get_mongo_id
    return BSON::ObjectId.new
  end
end
