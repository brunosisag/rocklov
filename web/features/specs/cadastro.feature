#language: pt

Funcionalidade: Cadastro
	Sendo um músicoque possui esquipamentos musicais
	Quero fazer o meu cadastro no RockLov
	Para que eu possa disponibilizá-los para locação

	@cadastro
	Cenario: Fazer cadastro
		Dado que o acesso a página de cadastro
		Quando submeto o seguinte formulário de cadastro:
			| nome    | email                | senha  |
			| Roberto | "fernando@gmail.com" | pwd123 |
		Então sou redirecionado para o Dashboard

	@validacadastro
	Esquema do Cenario: Tentativa de Cadastro

		Dado que o acesso a página de cadastro
		Quando submeto o seguinte formulário de cadastro:
			| nome         | email         | senha         |
			| <nome_input> | <email_input> | <senha_input> |
		Então vejo a mensagem de aleta: "<mensagem_output>"

		Exemplos:
			| nome_input   | email_input     | senha_input | mensagem_output                  |
			|              | bruno@gmail.com | pwd123      | Oops. Informe seu nome completo! |
			| Bruno Araujo |                 | pwd123      | Oops. Informe um email válido!   |
			| Bruno Araujo | bruno*gmail.com | pwd123      | Oops. Informe um email válido!   |
			| Bruno Araujo | bruno&gmail.com | pwd123      | Oops. Informe um email válido!   |
			| Bruno Araujo | bruno@gmail.com |             | Oops. Informe sua senha secreta! |