#language: pt

Funcionalidade: Login
	Sendo um usuário cadastrado
	Quero acessar o sistema da RockLov
	Para que eu possa anunciar meus equipamentos musicais

	Cenario: Login do usuário
		Dado que acesso a página principal
		Quando submeto minhas credenciais com "<email_imput>""<senha_imput>"
		Então sou redirecionado para o Dashboard

		Exemplos:
			| email_imput     | senha_imput |
			| bruno@gmail.com | pwd123      |


	Esquema do Cenario: Tentar logar

		Dado que acesso a página principal
		Quando submeto minhas credenciais com "<email_imput>""<senha_imput>"
		Então vejo a mensagem de aleta: "<mensagem_output>"

		Exemplos:
			| email_imput          | senha_imput | mensagem_output                  |
			| brunosisag@gmail.com | pwd12       | Usuário e/ou senha inválidos.    |
			| bruno@gmail.com      | pwd123      | Usuário e/ou senha inválidos.    |
			| brunogmail.com       | pwd123      | Oops. Informe um email válido!   |
			|                      | pwd123      | Oops. Informe um email válido!   |
			| brunosisag@gmail.com |             | Oops. Informe sua senha secreta! |