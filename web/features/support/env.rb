require "allure-cucumber"
require "capybara"
require "capybara/cucumber"
require "faker"


CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))

case ENV["BROWSER"]
when "firefox"
  @driver = :selenium
when "chrome"
  @driver = :selenium_chrome
when "fire_headless"
  @driver = :selenium_headless
when "chrome_headless"
  @driver = :selenium_chrome_headless
else
  # para estourar uma excessão
  raise "Navegador incorreto, variável @driver está vazia :("
end

Capybara.configure do |config|
  config.default_driver = @driver
  config.app_host = CONFIG["url"]
  # Com essa configuração o capybara espera 5 segundo para encontrar o elemento.
  config.default_max_wait_time = 10
end

AllureCucumber.configure do |config|
  config.results_directory = "/logs"
  # esse comando faz com que o allure semrpe apague o log criando anteriormente e crie um novo
  config.clean_results_directory = true
end
