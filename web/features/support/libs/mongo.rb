require "mongo"

Mongo::Logger.logger = Logger.new("./logs/mongo.log")

class MongoDB
  attr_accessor :db_client, :users, :equipos

  def initialize
    @db_client = Mongo::Client.new(CONFIG["mongo"])
    @users = db_client[:users]
    @equipos = db_client[:equipos]
  end

  def drop_danger
    @db_client.database.drop
  end

  def insert_user(docs)
    #inseri todos os usuaruis que estão na lista
    @users.insert_many(docs)
  end

  def remove_user(email)
    @users.delete_many({ email: email })
  end

  def get_user(email)
    user = @users.find({ email: email }).first
    return user[:_id]
  end

  def remove_equipo(name, email)
    user_id = get_user(email)
    @equipos.delete_many({ name: name, user: user_id })
  end
end
